FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential wget
RUN pip install pipenv
COPY . /app
WORKDIR /app
RUN pipenv install --system --deploy
RUN apt-get install -y chromium-chromedriver
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["app.py"]
