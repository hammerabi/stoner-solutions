from flask import Flask
from poll import EdiblesChecker
app = Flask(__name__)

@app.route('/')
def check():
    edibles_checker = EdiblesChecker()
    edibles_checker.get_products()
    return "Success"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
