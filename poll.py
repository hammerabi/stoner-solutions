import requests
from twilio.rest import Client
from bs4 import BeautifulSoup
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class EdiblesChecker():
    """
    Class designed to poll different dispensaries in Halifax
    and send a text message to Tassio and I when they
    have weed edibles in stock. Uses beautifulsoup for web scraping,
    and Twilio for text message support.
    """
    def __init__(self):
        self.base_url = "https://weedmaps.com/dispensaries/"
        self.dispensaries = [
            "canna-clinic-dresden-row"
        ]
        self.key_words = [
            "chocolate",
            "chocolates",
            "gummies",
            "gummy",
            "jelly",
            "lollipop",
            "brownies",
        ]
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        self.browser = webdriver.Chrome(chrome_options=options)
        account_sid = 'AC13bcf7298420e1692ac0b34043765ee2'
        auth_token = 'f84f7d1ee3f8fa46010eb04abad6e6d7'
        self.client = Client(account_sid, auth_token)

    def get_products(self):
        """
        Retrieves the products for a given dispensary.
        """
        products = self.scroll_and_parse()
        if products:
            self.send_text(products)

    def scroll_and_parse(self):
        """
        Iterates through each dispensary defined in the class,
        and parses their products page looking for key words
        of interest.
        """
        found_products = {}
        for dispensary in self.dispensaries:
            self.browser.get("{base}{dispensary}".format(
                base=self.base_url,
                dispensary=dispensary
            ))
            time.sleep(1)

            elem = self.browser.find_element_by_tag_name("body")

            no_of_pagedowns = 200

            while no_of_pagedowns:
                elem.send_keys(Keys.PAGE_DOWN)
                time.sleep(2)
                no_of_pagedowns-=1

            items = self.browser.find_elements_by_tag_name("li")
            products = []
            for item in items:
                products.append(item.text)

            for product in products:
                item_lowered = [text.lower() for text in product.split()]
                for key_word in self.key_words:
                    if key_word in item_lowered:
                        found_products[key_word] = product
        return found_products

    def send_text(self, products):
        text_string = ""
        for key, value in products.items():
            text_string += "{key}: {value}\n".format(
                key=key,
                value=value
            )
        message = self.client.messages \
                        .create(
                             body=text_string,
                             from_='+18677940546',
                             to='+19055990970'
                         )

if __name__ == "__main__":
    checker = EdiblesChecker()
    checker.get_products()
